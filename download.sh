#!/bin/sh

#下载全球IP地址分配情况，并过滤出中国网段
wget -q --timeout=60 -O- 'http://ftp.apnic.net/apnic/stats/apnic/delegated-apnic-latest' | grep "CN|ipv4" -- | awk --field-separator='|' '{ printf("%s/%d\n", $4, 32-log($5)/log(2)) }' > /root/allow_china_only/china_ssr.txt

